from aiy.board import Board, Led
import subprocess
from wit import Wit
import wikipedia
from secret import token
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from time import sleep, time
import os

client = Wit(token.API_TOKEN)

chrome_options = Options()
# chrome_options.add_argument("--headless")
# chrome_options.add_argument("user-data-dir=~/.config/chromium/Default")
driver = webdriver.Chrome(chrome_options=chrome_options)
# driver = webdriver.Chrome()


def context(query):
    query_list = query[0].split()

    print(query_list)

    if "you" and "tube" in query_list[0]:
        youtube(" ".join(query_list[2:]))

    if "wikipedia" in query_list:
        query_result = str(wikipedia.summary(query_list[1]).split('\n')[0])

        print(query_result)

        text_to_speech((query_result, query[1]))


def youtube(query):
    driver.get(f"https://duckduckgo.com/html/?q=site:youtube.com+{query}+video+song&t=h_&iar=videos&iax=videos&ia=videos")

    text_to_speech(f"playing {query}")

    try:
        suggestion = driver.find_element_by_xpath("//*[@id=\"did_you_mean\"]")

        if "Did you mean" in suggestion.text:
            driver.find_element_by_xpath("// *[ @ id = \"links\"] / div[2] / div / h2 / a").click()

    except expected_conditions.NoSuchElementException:
        driver.find_element_by_xpath("// *[ @ id = \"links\"] / div[1] / div / h2 / a").click()


    try:
        WebDriverWait(driver, 10).until(expected_conditions.presence_of_element_located
                                       ((By.CLASS_NAME, 'ytp-time-duration')))
        print("i guess it works")
    except Exception as e:
        print(f'Something broke and you should feel ashamed.... {e}')

    # JS for video duration
    video_time = driver.execute_script("return document.querySelectorAll('video')[0]['duration']")

    print(video_time)
    sleep(video_time)
    driver.get('about:blank')


def voice_input():
    with Board() as board:
        resp = None

        board.button.wait_for_press()
        board.led.state = Led.ON
        start_time = time()
        audio_rec = subprocess.Popen(['arecord', '-t', 'wav', '-f', 'cd', 'test.wav'], stdout=subprocess.PIPE, shell=False)
        board.button.wait_for_release()
        board.led.state = Led.OFF
        audio_rec.kill()
        end_time = time()

        length = end_time - start_time

        os.chmod('test.wav', 0o775)

        with open('test.wav', 'rb') as f:
            resp = client.speech(f, None, {'Content-Type': 'audio/wav'})

    voice_response = str(resp['_text'])

    print(f"Voice: {length}")

    return voice_response, int(length)


def text_to_speech(query):
    with open('tts.txt', 'w') as f:
        f.write(query[0])

    print(f"pure query: {query}")

    # this works because of owenz! <3
    subprocess.call(['espeak-ng', '-f', 'tts.txt', '-w', 'tts.wav'])
    sleep(5)
    subprocess.call(['aplay', 'tts.wav'])


while True:
    print("Press button, get noises!")
    context(voice_input())

# context('wikipedia lemons')
