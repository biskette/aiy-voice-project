from aiy.board import Board, Led
import subprocess
from wit import Wit
from secret import token
from selenium import webdriver
import os

client = Wit(token.API_TOKEN)


def youtube(query):
    options = webdriver.ChromeOptions().add_argument("--headless")
    driver = webdriver.Chrome(chrome_options=options)

    driver.get(f"https://duckduckgo.com/?q={query}&t=h_&iar=videos&iax=videos&ia=videos")
    driver.find_element_by_css_selector("#zci-videos > div > div.tile-wrap > div > div:nth-child(3)").click()
    driver.find_element_by_css_selector("""#zci-videos > div.detail.detail--slider.detail--videos.detail--xd > div > 
                                        div.detail__panes.js-detail-panes > div:nth-child(1) > div > 
                                        div.detail__media.detail__media--vid.js-video > div > div > div > div > ul > 
                                        li:nth-child(2) > a
                                        """).click()


def voice_input():
    with Board() as board:
        resp = None

        print('Tell me your song request!')

        board.button.wait_for_press()
        board.led.state = Led.ON
        audio_rec = subprocess.Popen(['arecord', '-t', 'wav', 'test.wav'], stdout=subprocess.PIPE, shell=False)
        board.button.wait_for_release()
        board.led.state = Led.OFF
        subprocess.Popen.kill(audio_rec)

        os.chmod('test.wav', 0o775)

        with open('test.wav', 'rb') as f:
            resp = client.speech(f, None, {'Content-Type': 'audio/wav'})

    print(str(resp['_text']))
    youtube(str(resp['_text']))


voice_input()
